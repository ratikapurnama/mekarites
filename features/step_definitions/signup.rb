require 'selenium-webdriver'
require 'rubygems'
require 'rspec'

driver = Selenium::WebDriver.for:chrome

Given("open the website amazon") do
  driver.navigate.to "https://www.amazon.com/"
end

When("user click on Login") do
  driver.find_element(:id,'nav-link-accountList').click
end

And("user click on signup") do
  driver.find_element(:id,'createAccountSubmit').click
end

And("user input name") do
  driver.find_element(:id,'ap_customer_name').send_keys 'lula'
end

And("user input email") do
  driver.find_element(:id, 'ap_email').send_keys 'lula@gmail.com'
end

And("user input password") do
  driver.find_element(:id, 'ap_password').send_keys 't35t1n9t35t1n9t35t1n9t35t1n9'
end

And("user input re-enter password") do
  driver.find_element(:id, 'ap_password_check').send_keys 't35t1n9t35t1n9t35t1n9t35t1n9'
end

And("user click button continue") do
    driver.find_element(:id, 'continue').click
end

Then ("User should open verify page") do
    driver.find_element(:xpath, '//head > title')
end
    
Then("user closing the browser") do
    driver.quit
end
